import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

//Component
import Welcome from '../Welcome'

const Routes = () => (
  <Router>
    <Route exact name="index" path="/" component={Welcome} />
  </Router>
)

export default Routes;
