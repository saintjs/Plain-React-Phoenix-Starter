import React, {Component} from 'react'

class Welcome extends Component {
  render(){
    return(
      <div>
        <h2>Team-GSD</h2>
        <p>This is our React + Phoenix Starter</p>
        <h2>What's Included:</h2>
        <ul>
          <li>Elixir Phoenix</li>
          <li>React</li>
          <li>React Router v4</li>
          <li>Mariaex (MySQL dependencies for Phoenix)</li>
        </ul>
        <h2>Who are we?</h2>
        <p>We're just a bunch of newbies who wanted to become Great Programmer oneday.</p>
        <p><a href="https://team-gsd.com">Visit Us!</a></p>
      </div>
    )
  }
}
export default Welcome;
